# Christmas Train Kata
## Credits
Originally by [Jason Gorman and Codemanship](https://codemanship.wordpress.com/2019/12/18/the-xmas-train-kata/)

![A train in wintertime](./img/polar_express.jpg)
## The Problem
Imagine we run a railway that Santa and his helpers use to ship presents from their factory in Lapland non-stop to a 
distribution depot 860 km away just outside Helsinki. (That’s how it works. Don’t argue.)

The elves working at the depot need to know what time to expect the train so they can make sure everything’s ready at 
their end – reindeer fed, sleigh oiled, Santa’s lunchbox packed, etc. To aid in improving the accuracy of their ETA, 
they have commissioned us to write a software system.

Positioned at intervals of about 1 km, they’ve placed sensors along the track that send a signal to the depot as the 
train passes. There are contact points at the front and the rear of the train, so each passing of a sensor triggers 2 
data messages – front and rear. The train is 200 m long.

Included in each data message is location information about that particular sensor telling us how far along the track 
from the factory in Lapland it is in kilometres.

The elves, of course, insist on a JSON format for these data messages, which looks like this:

**First Message**
<pre>
{
    'passing': {
        'datetime': '2019-12-24T19:37:46.854Z',
        'contact': 'front',
        'distance': '183.449 km'
    }
}
</pre>

**Second Message**
<pre>
{
    'passing': {
        'datetime': '2019-12-24T19:37:52.229Z',
        'contact': 'rear',
        'distance': '183.449 km'
    }
}
</pre>

Our software must determine the time that has elapsed between the first and second message, and use that information to 
calculate the speed of the train as it passed that sensor, and from there calculate an estimated time of arrival at the 
depot to the nearest minute.

This estimate will be updated with every new pair of messages sent by each sensor along the track to give the elves a 
live picture of the progress of the train.

Although in real life the train would need to vary its speed depending on conditions, for this exercise assume the 
train accelerates from rest at a constant 1 m/s/s and decelerates at the same rate, and has a maximum speed of 150 km/h.

For this exercise, you will need to create two programs:

* One that simulate’s the train’s journey, sending pairs of sensor-passing data messages at intervals approximating 
every kilometre of the train’s progress
* Another to receive these messages and update the train’s estimated time of arrival and display it on the screen
* The sensors will send one final message when the train has reached the depot to let the receiver know the journey has 
ended

Best of luck. The elves are counting on you!